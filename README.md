# Math test

This type of math works: $` y = x^2 `$

This type of math does not work:

```math
y = x^2
```

It's not working even in documentation: 

* https://docs.gitlab.com/ee/user/markdown.html#math